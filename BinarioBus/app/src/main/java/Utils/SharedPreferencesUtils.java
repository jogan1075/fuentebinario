package Utils;

import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;

import com.jmc.binariobus.ApplicationContext;

/**
 * Created by jogan1075 on 30-03-17.
 */

public class SharedPreferencesUtils {

    private final static String FINGERPRINT = "fingerprint";
    private static final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ApplicationContext.getInstance().getApplicationContext());
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";



    public static Location getLastKnownLocation(){
        double latitude = Double.valueOf(preferences.getString(LATITUDE, "0"));
        double longitude = Double.valueOf(preferences.getString(LONGITUDE, "0"));
        if(longitude == 0 && latitude == 0){
            return null;
        }
        Location location = new Location("");
        location.setLongitude(longitude);
        location.setLatitude(latitude);
        return location;
    }

    public static void setLasKnownLocation(Location location){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LATITUDE, String.valueOf(location.getLatitude()));
        editor.putString(LONGITUDE, String.valueOf(location.getLongitude()));
        editor.commit();
    }

    /**
     * Guarda la preferencia
     * @param key Clave de la preferencia
     * @param value Valor
     */
    public static void savePreference(String key, String value){
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putString(key, value);
        preferencesEditor.commit();
    }


    public static void savePreference(String key, int value) {
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putInt(key, value);
        preferencesEditor.commit();
    }
    public static void savePreference(String key, long value) {
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putLong(key, value);
        preferencesEditor.commit();
    }
    public static void removePreference(String key){
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.remove(key);
        preferencesEditor.commit();
    }

    /**
     * @param key Clave de la preferencia
     * @param defaultValue Valor por defecto
     * @return Retorna el valor de la preferencia de clave key. Si no la encuentra,
     * retorna defaultValue
     */
    public static String getPreference(String key, String defaultValue){
        return preferences.getString(key, defaultValue);
    }


    public static int getPreference(String key, int defaultValue){
        return preferences.getInt(key, defaultValue);
    }

    /**
     * Guarda la preferencia
     * @param key Clave de la preferencia
     * @param value Valor
     */
    public static void savePreference(String key, boolean value){
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putBoolean(key, value);
        preferencesEditor.commit();
    }

    /**
     * @param key Clave de la preferencia
     * @param defaultValue Valor por defecto
     * @return Retorna el valor de la preferencia de clave key. Si no la encuentra,
     * retorna defaultValue
     */
    public static boolean getPreference(String key, boolean defaultValue){
        return preferences.getBoolean(key, defaultValue);
    }

}



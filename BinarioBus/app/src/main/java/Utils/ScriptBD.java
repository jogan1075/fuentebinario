package Utils;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import MOD.BoletoMOD;
import MOD.ConfigMOD;
import MOD.TarifasMOD;

/**
 * Created by jogan1075 on 24-10-17.
 */

public class ScriptBD {
    public static final String Script_Delete_Table = "DROP TABLE IF EXISTS ";

    public boolean createDB(SQLiteDatabase db) {
        boolean ret = false;
        try {

            db.execSQL(TarifasMOD.SCRIPT_CREACION_TABLA_TARIFAS);
            db.execSQL(BoletoMOD.SCRIPT_CREACION_TABLA_BOLETOS);
//            db.execSQL(AreasMOD.SCRIPT_CREACION_TABLA_AREAS);
//            db.execSQL(InstalacionesMOD.SCRIPT_CREACION_TABLA_INSTALACIONES);
            db.execSQL(ConfigMOD.SCRIPT_CREACION_TABLA_CONFIG);

            ret = true;
        } catch (Exception e) {
            Log.e("Exception CreateDB", e.getMessage());
        }

        return ret;
    }

    public boolean dropTable(SQLiteDatabase db) {
        boolean ret = false;

        try {
            db.execSQL(Script_Delete_Table + TarifasMOD.NOMBRE_TABLE_TARIFAS);
            db.execSQL(Script_Delete_Table + BoletoMOD.NOMBRE_TABLE_BOLETOS);
//            db.execSQL(Script_Delete_Table + ContratistaMOD.NOMBRE_TABLE_CONTRATISTAS);
//            db.execSQL(Script_Delete_Table + AreasMOD.NOMBRE_TABLE_AREAS);
//            db.execSQL(Script_Delete_Table + InstalacionesMOD.NOMBRE_TABLE_INSTALACIONES);
            db.execSQL(Script_Delete_Table + ConfigMOD.NOMBRE_TABLE_CONFIG);

            ret = true;
        } catch (Exception e) {
            Log.d("Exception CreateDB", e.getMessage());
        }

        return ret;
    }
}

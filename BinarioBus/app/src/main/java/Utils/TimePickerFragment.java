package Utils;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.jmc.binariobus.R;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by jogan1075 on 31-10-17.
 */

public class TimePickerFragment extends DialogFragment {

    private TimePicker timePicker;

    public interface TimeDialogListener {
        void onFinishDialog(String time);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_time, null);

        timePicker = (TimePicker) v.findViewById(R.id.dialog_time_picker);
        timePicker.setIs24HourView(true);
        return new android.support.v7.app.AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("")
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int hour = 0;
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                    hour = timePicker.getHour();
                                } else {
                                    hour = timePicker.getCurrentHour();
                                }
                                int minute = 0;
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                    minute = timePicker.getMinute();
                                } else {
                                    minute = timePicker.getCurrentMinute();
                                }
                                TimeDialogListener activity = (TimeDialogListener) getActivity();
                                activity.onFinishDialog(updateTime(hour, minute));
                                dismiss();
                            }
                        })
                .create();
    }

    private String updateTime(int hours, int mins) {

        String hora = "";
        if (hours < 10)
            hora = "0" + hours;
        else
            hora = String.valueOf(hours);

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        String myTime = new StringBuilder().append(hora).append(':')
                .append(minutes).append(":00").toString();

        return myTime;
    }
}


package Utils;

/**
 * Connect to api.
 *
 * @author jogan1075
 * @version 1.4.0 
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class ConnectMySQL
{
	private final String PAGETAG = "ConnectMySQL";
	
	private Connection connect = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	
	public static final String MYSQL_IP = "binariobus.cl";
	public static final String MYSQL_DBNAME = "binariob_binario";
	public static final String MYSQL_USERNAME = "binariob_randami";
	public static final String MYSQL_PASSWORD = "Binario123**";
	
	
	
	/* close connect */
	private void close()
	{
		try
		{
			if (resultSet != null)
			{
				resultSet.close();
			}
			
			if (statement != null)
			{
				statement.close();
			}
			
			if (connect != null)
			{
				connect.close();
			}
		}
		catch (Exception e)
		{
			
		}
	}
	
	
}

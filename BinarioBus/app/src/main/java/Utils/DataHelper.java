package Utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jogan1075 on 24-10-17.
 */

public class DataHelper extends SQLiteOpenHelper {
    private static DataHelper instance;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "BDBinarioBus.db";
    private ScriptBD _scriptBD;

    private DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        _scriptBD = new ScriptBD();
    }

    public static DataHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DataHelper(context);
        }
        return instance;
    }

    public DataHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                      int version) {
        super(context, name, factory, version);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        _scriptBD.createDB(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        _scriptBD.dropTable(db);
        onCreate(db);
    }

}

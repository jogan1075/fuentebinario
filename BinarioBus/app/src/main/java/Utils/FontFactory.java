package Utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;

/**
 * Created by jogan1075 on 05-07-17.
 */

public class FontFactory {
    private static FontFactory instance;
    private HashMap<String, Typeface> fontMap = new HashMap<>();

    private FontFactory() {
    }

    public static FontFactory getInstance() {
        if (instance == null) {
            instance = new FontFactory();
        }
        return instance;
    }

    public Typeface getFont(Context context, String font) {
        Typeface typeface = fontMap.get(font);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Fonts/" + font);
            fontMap.put(font, typeface);
        }
        return typeface;
    }

//    public Typeface getThinFont(Context context) {
//        String font = "PFBeauSansPro-Thin.otf";
//        Typeface typeface = fontMap.get(font);
//        if (typeface == null) {
//            typeface = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/" + font);
//            fontMap.put(font, typeface);
//        }
//        return typeface;
//    }
//
//    public Typeface getLightFont(Context context) {
//        String font = "PFBeauSansPro-Light.otf";
//        Typeface typeface = fontMap.get(font);
//        if (typeface == null) {
//            typeface = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/" + font);
//            fontMap.put(font, typeface);
//        }
//        return typeface;
//    }

    public Typeface getNormalFont(Context context) {
        String font = "verdana.ttf";
        Typeface typeface = fontMap.get(font);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Fonts/" + font);
            fontMap.put(font, typeface);
        }
        return typeface;
    }

    public Typeface getItalicdFont(Context context) {
        String font = "verdana_italic.ttf";
        Typeface typeface = fontMap.get(font);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Fonts/" + font);
            fontMap.put(font, typeface);
        }
        return typeface;
    }

    public Typeface getBoldFont(Context context) {
        String font = "verdana_bold.ttf";
        Typeface typeface = fontMap.get(font);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Fonts/" + font);
            fontMap.put(font, typeface);
        }
        return typeface;
    }

//    public Typeface getExtraThinFont(Context context) {
//        String font = "PFBeauSansPro-XThin.otf";
//        Typeface typeface = fontMap.get(font);
//        if (typeface == null) {
//            typeface = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/" + font);
//            fontMap.put(font, typeface);
//        }
//        return typeface;
//    }
}

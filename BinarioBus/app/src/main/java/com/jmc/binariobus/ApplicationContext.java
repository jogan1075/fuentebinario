package com.jmc.binariobus;

import android.content.Intent;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.util.List;

import MOD.BoletoMOD;

/**
 * Created by jogan1075 on 30-03-17.
 */

public class ApplicationContext extends android.app.Application {
    private static ApplicationContext instance;

    public static Intent ServicioUbicacionActual;
    Double latutid;
    Double longitud;
    String provider;

    List<String> dates;
    BoletoMOD dataBoleto;

    String valorTotal;

    public String getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(String valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BoletoMOD getDataBoleto() {
        return dataBoleto;
    }

    public void setDataBoleto(BoletoMOD dataBoleto) {
        this.dataBoleto = dataBoleto;
    }

    public List<String> getDates() {
        return dates;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }

    public Double getLatutid() {
        return latutid;
    }

    public void setLatutid(Double latutid) {
        this.latutid = latutid;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        instance = this;
//        ServicioUbicacionActual = new Intent(this, Posicion.class);
//        startService(ServicioUbicacionActual);
    }

    public static android.content.Context getContext() {
        return instance;
    }

    public static ApplicationContext getInstance() {
        return instance;
    }


}




package com.jmc.binariobus;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.printer.UsbThermalPrinter;

import Utils.FontFactory;
import Utils.SharedPreferencesUtils;
import Utils.Utilidades;

public class MenuPrincipal extends Activity {

    UsbThermalPrinter mUsbThermalPrinter = new UsbThermalPrinter(ApplicationContext.getContext());





    String printContent;


    private boolean LowBattery = false;


    private final int LOWBATTERY = 4;

    private final int PRINTBARCODE = 6;
    private final int PRINTQRCODE = 7;
    private final int PRINTPAPERWALK = 8;


    private final int MAKER = 13;
    private final int PRINTPICTURE = 14;
    private final int NOBLACKBLOCK = 15;

    public static int paperWalk;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);


        TextView textView4 = (TextView)findViewById(R.id.textView4);
        TextView textView2 = (TextView)findViewById(R.id.textView2);
        TextView textView5 = (TextView)findViewById(R.id.textView5);
        TextView textView6 = (TextView)findViewById(R.id.textView6);

        textView6.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        textView4.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        textView2.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        textView5.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));




    }



    public void btnEliminarBoleto(View v) {
        startActivity(new Intent(this, DeleteBoleto.class));
        finish();
    }

    public void btnVenderBoleto(View v) {

        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Iniciar Recorrido");
        dialogo1.setMessage("¿Desea Iniciar el Recorrido? al iniciar, se trabajara en modo offline, al finalizar recorrido, se sincronizara con servidor");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Iniciar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        startActivity(new Intent(MenuPrincipal.this, VenderBoleto.class));
                        finish();
                    }
                });
        dialogo1.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        dialogo1.dismiss();
                    }
                });
        dialogo1.show();


    }

    public void btnPrintBeetweenBoleto(View v) {
        startActivity(new Intent(this, SelectFechaHora.class));
        finish();



    }

    public void btnCerrarSession(View v) {
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Cerrar Session!");
        dialogo1.setMessage("¿Realmente desea Cerrar la session?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("SI",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        startActivity(new Intent(getApplicationContext(), Login.class));
                        finish();
                    }
                });
        dialogo1.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        dialogo1.dismiss();
                    }
                });
        dialogo1.show();
    }

    public void onBackPressed() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_HOME
                && event.getRepeatCount() == 0) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
//        if (progressDialog != null && !MenuPrincipal.this.isFinishing()) {
//            progressDialog.dismiss();
//            progressDialog = null;
//        }
////        mUsbThermalPrinter.stop();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }



}

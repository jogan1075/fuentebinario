package com.jmc.binariobus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import DAO.UsuarioDAO;
import Utils.ConnectMySQL;
import Utils.FontFactory;
import Utils.SharedPreferencesUtils;


import static Utils.Utilidades.isOnline;

public class Login extends AppCompatActivity {

    EditText editUser;
    EditText editPass;
    ConnectMySQL mysql;
    UsuarioDAO _usuarioDAO;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences = getApplicationContext().getSharedPreferences("token", Context.MODE_PRIVATE);
        editUser = (EditText) findViewById(R.id.editText2);
        editPass = (EditText) findViewById(R.id.editText3);

        editUser.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        editPass.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        editUser.setText("demo@demo.cl");
        editPass.setText("demo123");
        mysql = new ConnectMySQL();
        _usuarioDAO = UsuarioDAO.getInstance(this);
    }

    public void BtnEntrarLogin(View v) {


        if(editUser.getText().toString().equalsIgnoreCase("admin") && editPass.getText().toString().equalsIgnoreCase("1234admin4321")){
            startActivity(new Intent(getApplicationContext(), configuracion.class));
            finish();
        }else{
            new LoginAsync().execute();
        }


    }

    public class LoginAsync extends AsyncTask<String, Void, String> {
        ProgressDialog pdCarga;
        Boolean resp = false;
        String MSG = "";

        UsuarioDAO _usuarioDAO;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pdCarga = new ProgressDialog(com.jmc.binariobus.Login.this);
            pdCarga.setTitle("Procesando...");
            pdCarga.setMessage("Por favor espere...");
            pdCarga.setCancelable(false);
            pdCarga.setIndeterminate(true);
            pdCarga.show();

            _usuarioDAO = UsuarioDAO.getInstance(getApplicationContext());
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub


            try {

                if (isOnline(getApplicationContext())) {

                    if (_usuarioDAO.LoginUsuario(editUser.getText().toString(), editPass.getText().toString())) {
                        resp = true;
                    } else {
                        MSG = "Usuario o Contraseña mal ingresada";

                    }
                } else {
                    // pdCarga.dismiss();
                    MSG = "Problemas de conexion, verifique y vuelva intentar..";

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pdCarga.dismiss();
            if (resp) {
                SharedPreferencesUtils.savePreference("isLogin",true);
                startActivity(new Intent(getApplicationContext(), MenuPrincipal.class));
                finish();
            } else if (!MSG.equals("")) {

                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(Login.this);
                dialogo1.setTitle("Error!");
                dialogo1.setMessage(MSG);
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Acepta",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                dialogo1.dismiss();

                            }
                        });
                dialogo1.show();
            }

        }

    }

    public void onBackPressed() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botï¿½n al pulsar ir a atrï¿½s

            return true;
        } else if (keyCode == KeyEvent.KEYCODE_HOME
                && event.getRepeatCount() == 0) {

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setfuncview();
//        Toast.makeText(this, "resume", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStop() {
        super.onStop();
//        Toast.makeText(this, "stop", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();

//        Toast.makeText(this, "pause", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void setfuncview() {
        if (sharedPreferences.getBoolean("first",true)) {
            sharedPreferences.edit().putBoolean("first",false).commit();
        }

    }

}

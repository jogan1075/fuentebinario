package com.jmc.binariobus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.location.LocationRequest;
//import com.google.android.gms.location.LocationServices;
//import com.google.android.gms.maps.model.LatLng;

import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.printer.UsbThermalPrinter;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import DAO.BoletosDAO;
import DAO.TarifasDAO;
import DAO.UsuarioDAO;
import MOD.BoletoMOD;
import MOD.TarifasMOD;
import Utils.GPSTracker;
import Utils.SharedPreferencesUtils;
import Utils.Utilidades;
import adapter.AdapterTarifas;

import static Utils.Utilidades.isOnline;

public class VenderBoleto extends AppCompatActivity {

//    private static final String TAG = "MainActivity";

    private String TAG = VenderBoleto.class.getSimpleName();
    private ListView listView;
    private AdapterTarifas adapter;
    TarifasMOD data;

    //variables de impresora bluetooh
    private static final boolean DEBUG = true;
    private String mConnectedDeviceName = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private static final int REQUEST_ENABLE_BT = 2;
    //    private BluetoothService mService = null;
    private static final int REQUEST_CONNECT_DEVICE = 1;

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONNECTION_LOST = 6;
    public static final int MESSAGE_UNABLE_CONNECT = 7;
    private static final String CHINESE = "GBK";
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    TarifasDAO _tarifasDAO;
    BoletosDAO _boletosDAO;
    GPSTracker gps;
    double latitude, longitude;
    List<TarifasMOD> lista;

    LinearLayout sincronizar;
//    private ProgressDialog progressDialog;


    UsbThermalPrinter mUsbThermalPrinter = new UsbThermalPrinter(ApplicationContext.getContext());
    MyHandler handler;
    private String printVersion;
    private final int PRINTVERSION = 5;
    private final int PRINTCONTENT = 9;
    private final int CANCELPROMPT = 10;
    private final int PRINTERR = 11;
    private final int OVERHEAT = 12;
    private final int NOPAPER = 3;
    ProgressDialog dialog;
    private ProgressDialog progressDialog;
    private int printGray;
    private int leftDistance = 0;
    private int lineDistance;
    private int wordFont;
    private String Result;
    private Boolean nopaper = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vender_boleto);

        listView = (ListView) findViewById(R.id.reciclador);

        sincronizar = (LinearLayout) findViewById(R.id.sincronizar);
        // verifica si el bluetooh esta activado
        _boletosDAO = BoletosDAO.getInstance(ApplicationContext.getContext());
        _tarifasDAO = TarifasDAO.getInstance(ApplicationContext.getContext());

        handler = new MyHandler();
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mUsbThermalPrinter.start(0);
                    mUsbThermalPrinter.reset();
                    printVersion = mUsbThermalPrinter.getVersion();
                } catch (TelpoException e) {
                    e.printStackTrace();
                } finally {
                    if (printVersion != null) {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.obj = "1";
                        handler.sendMessage(message);
                    } else {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.obj = "0";
                        handler.sendMessage(message);
                    }
                }
            }
        }).start();


        dialog = new ProgressDialog(VenderBoleto.this);
        dialog.setTitle("Cargando.");
        dialog.setMessage("espere por favor");
        dialog.setCancelable(false);
        dialog.show();

        lista = _tarifasDAO.getTarifasByIdVendedorLocal(SharedPreferencesUtils.getPreference("IdUsuario", null));
        adapter = new AdapterTarifas(this, R.layout.item_tarifa,
                lista);

        listView.setAdapter(adapter);

        gps = new GPSTracker(ApplicationContext.getContext());
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            gps.showSettingsAlert();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                data = (TarifasMOD) parent.getItemAtPosition(position);
                final String descripcion = data.getNombreTarifa().toString();
                final String valor = data.getValor().toString();
                final String accion = data.getAccion();
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(VenderBoleto.this);
                dialogo1.setTitle("Venta de Boleto");
                dialogo1.setMessage("¿Desea realizar la venta del boleto?");
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("SI",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {


                                String ultimoboleto = SharedPreferencesUtils.getPreference("ultimoBoletoVendido", null);


                                int cont = Integer.parseInt(ultimoboleto) + 1;

                                Boolean resp = _boletosDAO.insert(SharedPreferencesUtils.getPreference("IdUsuario", null), valor, Utilidades.getDateTimeNow2(), data.getId(), String.valueOf(longitude), String.valueOf(latitude), cont + "");

                                if (resp) {

                                    if (accion.equalsIgnoreCase("1")) {

                                        progressDialog = ProgressDialog.show(VenderBoleto.this, "", "cargando");
//                                            handler.sendMessage(handler.obtainMessage(PRINTCONTENT, 1, 0, null));
                                        try {
                                            mUsbThermalPrinter.reset();

//                mUsbThermalPrinter.setLeftIndent(leftDistance);
//                mUsbThermalPrinter.setLineSpace(lineDistance);
//                if (wordFont == 4) {
//                    mUsbThermalPrinter.setFontSize(2);
//                    mUsbThermalPrinter.enlargeFontSize(2, 2);
//                } else if (wordFont == 3) {
//                    mUsbThermalPrinter.setFontSize(1);
//                    mUsbThermalPrinter.enlargeFontSize(2, 2);
//                } else if (wordFont == 2) {
//                    mUsbThermalPrinter.setFontSize(2);
//                } else if (wordFont == 1) {
//                    mUsbThermalPrinter.setFontSize(1);
//                }
//                mUsbThermalPrinter.setGray(printGray);
                                            mUsbThermalPrinter.setAlgin(UsbThermalPrinter.ALGIN_MIDDLE);
                                            try {

                                                Bitmap mBitmap = null;
                                                URL url = new URL(SharedPreferencesUtils.getPreference("Logo", null));
                                                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                                connection.setDoInput(true);
                                                connection.connect();
                                                InputStream input = connection.getInputStream();
                                                mBitmap = BitmapFactory.decodeStream(input);
                                                mUsbThermalPrinter.printLogo(mBitmap, false);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                mUsbThermalPrinter.printLogo(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher), false);
                                            }


//                                            mUsbThermalPrinter.printLogo(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher), false);
                                            mUsbThermalPrinter.walkPaper(2);

                                            String msg = "DETALLE DE VENTA\n" +
                                                    "Num. Serie: " + cont + "\n" +
                                                    "Ruta: " + data.getNombreTarifa() + "\n" +
                                                    "Valor: $" + data.getValor() + "\n" +
                                                    "Fecha/Hora: " + Utilidades.getDateTimeNow() + "\n\n" +
                                                    "DETALLE DE BUS\n" +
                                                    "Auxiliar: " + SharedPreferencesUtils.getPreference("NombreCompleto", null) + "\n" +
                                                    "Num. Bus: " + SharedPreferencesUtils.getPreference("NroBus", null) + "\n\n" +
                                                    SharedPreferencesUtils.getPreference("politicaSeguridad",null) + "\n" +
                                                    "===========================" + "\n";
                                            mUsbThermalPrinter.setAlgin(UsbThermalPrinter.ALGIN_LEFT);
                                            mUsbThermalPrinter.addString(msg);
                                            mUsbThermalPrinter.printString();
                                            mUsbThermalPrinter.walkPaper(2);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Result = e.toString();
                                            if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                                                nopaper = true;
                                            } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                                                handler.sendMessage(handler.obtainMessage(OVERHEAT, 1, 0, null));
                                            } else {
                                                handler.sendMessage(handler.obtainMessage(PRINTERR, 1, 0, null));
                                            }
                                        } finally {
                                            handler.sendMessage(handler.obtainMessage(CANCELPROMPT, 1, 0, null));
                                            if (nopaper) {
                                                handler.sendMessage(handler.obtainMessage(NOPAPER, 1, 0, null));
                                                nopaper = false;
                                                return;
                                            }
                                        }

                                    }
                                } else {
                                    Utilidades.dialogErrorSimple(VenderBoleto.this, getResources().getString(R.string.title_msg_error_inesperado), getResources().getString(R.string.msg_error_inesperado));
                                }

                            }
                        });
                dialogo1.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {

                            }
                        });
                dialogo1.show();
            }
        });


        sincronizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utilidades.isOnline(ApplicationContext.getContext())) {
                    new SyncData().execute();

                } else {
                    AlertDialog.Builder dialogo1 = new AlertDialog.Builder(VenderBoleto.this);
                    dialogo1.setTitle("Error!");
                    dialogo1.setMessage("Debe Tener Conexion a internet para sincronizar las ventas del dia.");
                    dialogo1.setCancelable(false);
                    dialogo1.setPositiveButton("Aceptar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogo1, int id) {
                                    dialogo1.dismiss();

                                }
                            });
                    dialogo1.show();
                }
//                Toast.makeText(VenderBoleto.this, "test sincronizar", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case PRINTVERSION:
                    dialog.dismiss();
                    break;
                case PRINTCONTENT:
                    new contentPrintThread().start();
                    break;
                case CANCELPROMPT:
                    if (progressDialog != null && !VenderBoleto.this.isFinishing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                    break;
                default:
                    Toast.makeText(VenderBoleto.this, "Print Error!", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }


    private class contentPrintThread extends Thread {
        @Override
        public void run() {
            super.run();
            try {
                mUsbThermalPrinter.reset();
//                mUsbThermalPrinter.setAlgin(UsbThermalPrinter.ALGIN_LEFT);
//                mUsbThermalPrinter.setLeftIndent(leftDistance);
//                mUsbThermalPrinter.setLineSpace(lineDistance);
//                if (wordFont == 4) {
//                    mUsbThermalPrinter.setFontSize(2);
//                    mUsbThermalPrinter.enlargeFontSize(2, 2);
//                } else if (wordFont == 3) {
//                    mUsbThermalPrinter.setFontSize(1);
//                    mUsbThermalPrinter.enlargeFontSize(2, 2);
//                } else if (wordFont == 2) {
//                    mUsbThermalPrinter.setFontSize(2);
//                } else if (wordFont == 1) {
//                    mUsbThermalPrinter.setFontSize(1);
//                }
//                mUsbThermalPrinter.setGray(printGray);
//                mUsbThermalPrinter.setAlgin(UsbThermalPrinter.ALGIN_MIDDLE);
                mUsbThermalPrinter.printLogo(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher), false);
                mUsbThermalPrinter.walkPaper(2);

                String msg = "DETALLE DE VENTA\n" +
                        "Num. Serie: 01-000\n" +
                        "Ruta: asasasasasasasasas\n" +
                        "Valor: $14990\n" +
                        "Fecha/Hora: " + Utilidades.getDateTimeNow() + "\n\n" +
                        "DETALLE DE BUS\n" +
                        "Auxiliar: " + SharedPreferencesUtils.getPreference("NombreCompleto", null) + "\n" +
                        "Num. Bus: " + SharedPreferencesUtils.getPreference("NroBus", null) + "\n\n" +
                        getResources().getString(R.string.PoliticaSeguridad) + "\n" +
                        "================================" + "\n";

                mUsbThermalPrinter.addString(msg);
                mUsbThermalPrinter.printString();
                mUsbThermalPrinter.walkPaper(1);
            } catch (Exception e) {
                e.printStackTrace();
                Result = e.toString();
                if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                    nopaper = true;
                } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                    handler.sendMessage(handler.obtainMessage(OVERHEAT, 1, 0, null));
                } else {
                    handler.sendMessage(handler.obtainMessage(PRINTERR, 1, 0, null));
                }
            } finally {
                handler.sendMessage(handler.obtainMessage(CANCELPROMPT, 1, 0, null));
                if (nopaper) {
                    handler.sendMessage(handler.obtainMessage(NOPAPER, 1, 0, null));
                    nopaper = false;
                    return;
                }
            }
        }
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if (DEBUG)
            Log.e(TAG, "- ON PAUSE -");
    }


    @Override
    protected void onDestroy() {
        if (progressDialog != null && !VenderBoleto.this.isFinishing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        mUsbThermalPrinter.stop();
        super.onDestroy();
    }

//    void searchPrint() {
//        Intent serverIntent = new Intent(VenderBoleto.this, DeviceListActivity.class);
//        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
//    }

//
//    private void Print_BMP() {
//
//
////				return myBitmap;
//        } catch (IOException e) {
//            // Log exception
//            mBitmap = null;
//        }
//
//        int nMode = 0;
//        int nPaperWidth = 384;
////        if(width_58mm.isChecked())
//        nPaperWidth = 384;
////        else if (width_80.isChecked())
////            nPaperWidth = 576;
//        if (mBitmap != null) {
//            /**
//             * Parameters:
//             * mBitmap  要打印的图片
//             * nWidth   打印宽度（58和80）
//             * nMode    打印模式
//             * Returns: byte[]
//             */
//            byte[] data = PrintPicture.POS_PrintBMP(mBitmap, nPaperWidth, nMode);
//            //	SendDataByte(buffer);
//            SendDataByte(Command.ESC_Init);
//            SendDataByte(Command.LF);
//            SendDataByte(data);
//            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(30));
//            SendDataByte(PrinterCommand.POS_Set_Cut(1));
//            SendDataByte(PrinterCommand.POS_Set_PrtInit());
//        }
//    }
//
//
//
//    private void Print_Test() {
//        String msg = "DETALLE DE VENTA\n";
//        SendDataByte(PrinterCommand.POS_Print_Text(msg, CHINESE, 0, 1, 1, 0));
//        SendDataByte(PrinterCommand.POS_Set_Cut(1));
//        SendDataByte(PrinterCommand.POS_Set_PrtInit());
//    }
//
//    private void Print_Test_2() {
//        String msg = "DETALLE DE BUS\n";
//        SendDataByte(PrinterCommand.POS_Print_Text(msg, CHINESE, 0, 1, 1, 0));
//        SendDataByte(PrinterCommand.POS_Set_Cut(1));
//        SendDataByte(PrinterCommand.POS_Set_PrtInit());
//    }
//
//    private void Print_Test2(String numSerie, String descripcion, String valor) {
//        SendDataByte(PrinterCommand.POS_Print_Text("Num. Serie: 000" + numSerie + "\n", CHINESE, 0, 0, 0, 0));
//        SendDataByte(PrinterCommand.POS_Print_Text("Ruta: " + descripcion + "\n", CHINESE, 0, 0, 0, 0));
//        SendDataByte(PrinterCommand.POS_Print_Text("Valor: $" + valor + ".-" + "\n", CHINESE, 0, 0, 0, 0));
//        SendDataByte(PrinterCommand.POS_Print_Text("Fecha/Hora: " + Utilidades.getDateTimeNow() + "\n\n", CHINESE, 0, 0, 0, 0));
//        Print_Test_2();
//        SendDataByte(PrinterCommand.POS_Print_Text("Auxiliar: " + SharedPreferencesUtils.getPreference("NombreCompleto", null) + "\n", CHINESE, 0, 0, 0, 0));
//        SendDataByte(PrinterCommand.POS_Print_Text("Num. Bus: " + SharedPreferencesUtils.getPreference("NroBus", null) + "\n\n\n", CHINESE, 0, 0, 0, 0));
//        SendDataByte(PrinterCommand.POS_Print_Text(getResources().getString(R.string.PoliticaSeguridad) + "\n\n\n", CHINESE, 0, 0, 0, 0));
//        SendDataByte(PrinterCommand.POS_Print_Text("================================" + "\n\n\n", CHINESE, 0, 0, 0, 0));
//        SendDataByte(PrinterCommand.POS_Set_Cut(1));
//        SendDataByte(PrinterCommand.POS_Set_PrtInit());
//    }


    public void onBackPressed() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botï¿½n al pulsar ir a atrï¿½s

            startActivity(new Intent(this, MenuPrincipal.class));
            finish();

            return true;
        } else if (keyCode == KeyEvent.KEYCODE_HOME
                && event.getRepeatCount() == 0) {

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public class SyncData extends AsyncTask<String, Void, String> {
        ProgressDialog pdCarga;
        Boolean resp = false;
        String MSG = "";
        BoletosDAO boletosDAO;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pdCarga = new ProgressDialog(VenderBoleto.this);

            pdCarga.setMessage("Sincronizando con el servidor. Por favor espere...");
            pdCarga.setCancelable(false);
            pdCarga.setIndeterminate(true);
            pdCarga.show();

            boletosDAO = BoletosDAO.getInstance(ApplicationContext.getContext());
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub


            try {

                if (isOnline(getApplicationContext())) {

                    List<BoletoMOD> lista = boletosDAO.getAllBoletosLocales();

                    if (lista != null) {
                        for (int i = 0; i < lista.size(); i++) {

                            BoletoMOD data = lista.get(i);

                            Boolean resp = boletosDAO.InsertarBoleto(data.getIdUsuario(), data.getValorTarifa(), data.getFecha(), Integer.parseInt(data.getNombreTarifa())
                                    , data.getLongitud(), data.getLatitud(), data.getNumFolio(),data.getEliminado()+"");

                            if (resp) {
                                boletosDAO.deleteBoletoByID(data.getIdBoleto());
                            }

                        }
                    } else {
                        MSG = "Su dispositivo se encuentra Sincronizado con el servidor..";
                    }


                } else {
                    // pdCarga.dismiss();
                    MSG = "Problemas de conexion, verifique y vuelva intentar..";

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pdCarga.dismiss();
            if (!MSG.equals("")) {
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(VenderBoleto.this);
                dialogo1.setTitle("Error!");
                dialogo1.setMessage(MSG);
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                dialogo1.dismiss();

                            }
                        });
                dialogo1.show();
            } else {
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(VenderBoleto.this);
                dialogo1.setTitle("Sincronizacion Exitosa");
                dialogo1.setMessage("La sincronizacion ha finalizado con exito.");
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                dialogo1.dismiss();
                                startActivity(new Intent(VenderBoleto.this, MenuPrincipal.class));
                                finish();
                            }
                        });
                dialogo1.show();
            }

        }

    }

}

package com.jmc.binariobus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import DAO.ConfigDAO;
import Utils.FontFactory;

public class configuracion extends AppCompatActivity {

    EditText editHost, editNombre, editUser, editPass;
    TextView txthostBD, txtnombreBD, txtuserBD, txtpassBD;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);

        txthostBD = (TextView) findViewById(R.id.textView12);
        txtnombreBD = (TextView) findViewById(R.id.textView13);
        txtuserBD = (TextView) findViewById(R.id.textView14);
        txtpassBD = (TextView) findViewById(R.id.textView15);

        txthostBD.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        txtnombreBD.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        txtpassBD.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        txtuserBD.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));

        editHost = (EditText) findViewById(R.id.editText4);
        editNombre = (EditText) findViewById(R.id.editText5);
        editUser = (EditText) findViewById(R.id.editText6);
        editPass = (EditText) findViewById(R.id.editText7);


    }

    public void BtnGuardar(View v) {

        new SaveConfiguracion().execute();
    }

    public void BtnCancelar(View v) {
        startActivity(new Intent(this, Login.class));
        finish();
    }


    public class SaveConfiguracion extends AsyncTask<String, Void, String> {
        ProgressDialog pdCarga;
        Boolean resp = false;
        String MSG = "";

        ConfigDAO configDAO;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pdCarga = new ProgressDialog(configuracion.this);
            pdCarga.setTitle("Procesando...");
            pdCarga.setMessage("Por favor espere...");
            pdCarga.setCancelable(false);
            pdCarga.setIndeterminate(true);
            pdCarga.show();

            configDAO = ConfigDAO.getInstance(ApplicationContext.getContext());
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub


            try {

                if (configDAO.ActualizarConfig(1, editHost.getText().toString(), editNombre
                        .getText().toString(), editUser.getText().toString(), editPass
                        .getText().toString())) {

                    resp = true;
                } else {
                    resp = false;
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pdCarga.dismiss();


            if (resp) {
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(configuracion.this);
                dialogo1.setTitle("");
                dialogo1.setMessage("Datos Guardados con exito!");
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                dialogo1.dismiss();
                                startActivity(new Intent(configuracion.this, Login.class));
                                finish();
                            }
                        });
                dialogo1.show();
            } else {
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(configuracion.this);
                dialogo1.setTitle("Error!");
                dialogo1.setMessage("Hubo un problema al guardar los datos, intente nuevamente");
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                dialogo1.dismiss();
//                                startActivity(new Intent(configuracion.this, Login.class));
//                                finish();
                            }
                        });
                dialogo1.show();
            }


        }

    }

}

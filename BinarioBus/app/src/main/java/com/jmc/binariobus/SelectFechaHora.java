package com.jmc.binariobus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Utils.DatePickerFragment;
import Utils.DatePickerFragment2;
import Utils.TimePickerFragment;
import Utils.TimePickerFragment2;

public class SelectFechaHora extends AppCompatActivity implements DatePickerFragment.DateDialogListener, DatePickerFragment2.DateDialogListener, TimePickerFragment.TimeDialogListener, TimePickerFragment2.TimeDialogListener {
    private static final String DIALOG_DATE = "SelectFechaHora.DateDialog";

    EditText editdate, edittime, editdate2, edittime2;
    List<String> dates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_fecha_hora);

        editdate = (EditText) findViewById(R.id.editText10);
        edittime = (EditText) findViewById(R.id.editText11);
        editdate2 = (EditText) findViewById(R.id.editText8);
        edittime2 = (EditText) findViewById(R.id.editText9);

        editdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment dialog = new DatePickerFragment();
                dialog.show(getFragmentManager(), DIALOG_DATE);
            }
        });


        edittime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment dialog = new TimePickerFragment();
                dialog.show(getFragmentManager(), DIALOG_DATE);
            }
        });


        editdate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment2 dialog = new DatePickerFragment2();
                dialog.show(getFragmentManager(), DIALOG_DATE);
            }
        });


        edittime2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment2 dialog = new TimePickerFragment2();
                dialog.show(getFragmentManager(), DIALOG_DATE);
            }
        });
    }


    public void BtnBuscar(View v) {

        if (!editdate.getText().toString().isEmpty() &&
                !editdate2.getText().toString().isEmpty() &&
                !edittime.getText().toString().isEmpty() &&
                !edittime2.getText().toString().isEmpty()) {

            dates = new ArrayList<>();

            dates.add(editdate.getText().toString() + " " + edittime.getText().toString());
            dates.add(editdate2.getText().toString() + " " + edittime2.getText().toString());

            ApplicationContext.getInstance().setDates(dates);

            startActivity(new Intent(SelectFechaHora.this,DesdeHasta.class));
            finish();
        }else{
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(SelectFechaHora.this);
            dialogo1.setTitle("Error!");
            dialogo1.setMessage("Debe Ingresar toda la informacion para realizar la busqueda");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Aceptar",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            dialogo1.dismiss();

                        }
                    });
            dialogo1.show();
        }


    }

    public String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String hireDate = sdf.format(date);
        return hireDate;
    }


    @Override
    public void onFinishDialog(Date date) {
//        Toast.makeText(this, "Selected Date :" + formatDate(date), Toast.LENGTH_SHORT).show();
        editdate.setText(formatDate(date));

//        dates.add(formatDate(date));
    }

    @Override
    public void onFinishDialog(String time) {
        edittime.setText(time);
    }

    @Override
    public void onFinishDialog2(Date date) {
        editdate2.setText(formatDate(date));
    }

    @Override
    public void onFinishDialog2(String time) {
        edittime2.setText(time);
    }

    public void onBackPressed() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botï¿½n al pulsar ir a atrï¿½s

            startActivity(new Intent(this, MenuPrincipal.class));
            finish();

            return true;
        } else if (keyCode == KeyEvent.KEYCODE_HOME
                && event.getRepeatCount() == 0) {

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}

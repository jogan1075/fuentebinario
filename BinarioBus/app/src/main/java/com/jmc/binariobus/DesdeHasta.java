package com.jmc.binariobus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.printer.UsbThermalPrinter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import DAO.BoletosDAO;
import MOD.BoletoMOD;
import Utils.SharedPreferencesUtils;
import Utils.Utilidades;

import static Utils.Utilidades.isOnline;

public class DesdeHasta extends AppCompatActivity {
    List<String> dates;
    BoletoMOD boletoMOD;
    BoletosDAO boletosDAO;
    TextView description;

    UsbThermalPrinter mUsbThermalPrinter = new UsbThermalPrinter(ApplicationContext.getContext());
    MyHandler handler;
    private String printVersion;
    private final int PRINTVERSION = 5;
    private final int PRINTCONTENT = 9;
    private final int CANCELPROMPT = 10;
    private final int PRINTERR = 11;
    private final int OVERHEAT = 12;
    private final int NOPAPER = 3;
    ProgressDialog dialog;
    private ProgressDialog progressDialog;
    private int leftDistance = 0;
    private String Result;
    private Boolean nopaper = false;
    BoletoMOD dataBoleto;
    TextView total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desde_hasta);

        description = (TextView) findViewById(R.id.textView16);
        total = (TextView) findViewById(R.id.textView11);
        dates = new ArrayList<>();

        boletosDAO = BoletosDAO.getInstance(this);
        handler = new MyHandler();
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mUsbThermalPrinter.start(0);
                    mUsbThermalPrinter.reset();
                    printVersion = mUsbThermalPrinter.getVersion();
                } catch (TelpoException e) {
                    e.printStackTrace();
                } finally {
                    if (printVersion != null) {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.obj = "1";
                        handler.sendMessage(message);
                    } else {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.obj = "0";
                        handler.sendMessage(message);
                    }
                }
            }
        }).start();
        show();

        execConsulta(ApplicationContext.getInstance().getDates());
    }


    public void show() {
        dialog = new ProgressDialog(DesdeHasta.this);
        dialog.setTitle("Cargando.");
        dialog.setMessage("espere por favor");
        dialog.setCancelable(false);
        dialog.show();
    }


    public void BtnVerListado(View v) {
        startActivity(new Intent(this, ListadoBoletosVendidos.class));
        finish();
    }

    public void Imprimir(View v) {
        new PrintData().execute();
    }

    public String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String hireDate = sdf.format(date);
        return hireDate;
    }

    private void execConsulta(List<String> data) {
        if (Utilidades.isOnline(ApplicationContext.getContext())) {
            List<BoletoMOD> boletos = boletosDAO.BusquedaBoletosDesdeHasta(ApplicationContext.getInstance().getDates());

            description.setText("Se encontraron #" + boletos.size() + " registros.");

            int valores=0;
            for (int i = 0; boletos.size() > i; i++) {
                BoletoMOD dat = boletos.get(i);

                valores += Integer.parseInt(dat.getValorTarifa());

            }

            ApplicationContext.getInstance().setValorTotal(valores +"");

            total.setText("Total de ventas de $" + valores);

        } else {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(DesdeHasta.this);
            dialogo1.setTitle("Error!");
            dialogo1.setMessage("Se necesita conexión a internet, para poder realizar esta consulta de registros");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Aceptar",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            dialogo1.dismiss();

                        }
                    });
            dialogo1.show();
        }

    }


    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case PRINTVERSION:
                    dialog.dismiss();
                    break;
                case PRINTCONTENT:
//                    new contentPrintThread().start();
                    break;
                case CANCELPROMPT:
                    if (progressDialog != null && !DesdeHasta.this.isFinishing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                    break;
                default:
                    Toast.makeText(DesdeHasta.this, "Print Error!", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }


    @Override
    public synchronized void onPause() {
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        if (progressDialog != null && !DesdeHasta.this.isFinishing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        mUsbThermalPrinter.stop();
        super.onDestroy();
    }


    public class PrintData extends AsyncTask<String, Void, String> {
        ProgressDialog pdCarga;
        Boolean resp = false;
        String MSG = "";
        BoletosDAO boletosDAO;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pdCarga = new ProgressDialog(DesdeHasta.this);

            pdCarga.setMessage("Sincronizando con el servidor. Por favor espere...");
            pdCarga.setCancelable(false);
            pdCarga.setIndeterminate(true);
            pdCarga.show();

            boletosDAO = BoletosDAO.getInstance(ApplicationContext.getContext());
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub


            try {

                if (isOnline(getApplicationContext())) {

                    List<BoletoMOD> lista = boletosDAO.BusquedaBoletosDesdeHasta(ApplicationContext.getInstance().getDates());

                    if (lista != null) {

                        try {
                            mUsbThermalPrinter.reset();
                            mUsbThermalPrinter.setAlgin(UsbThermalPrinter.ALGIN_LEFT);

                            String msg =
                                    "Auxiliar: " +   SharedPreferencesUtils.getPreference("NombreCompleto", null);

                            mUsbThermalPrinter.addString(msg);
                            mUsbThermalPrinter.printString();
                            mUsbThermalPrinter.walkPaper(2);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Result = e.toString();
                            if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                                nopaper = true;
                            } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                                handler.sendMessage(handler.obtainMessage(OVERHEAT, 1, 0, null));
                            } else {
                                handler.sendMessage(handler.obtainMessage(PRINTERR, 1, 0, null));
                            }
                        } finally {
                            handler.sendMessage(handler.obtainMessage(CANCELPROMPT, 1, 0, null));
                            if (nopaper) {
                                handler.sendMessage(handler.obtainMessage(NOPAPER, 1, 0, null));
                                nopaper = false;

                            }
                        }

                        for (int i = 0; i < lista.size(); i++) {

                            dataBoleto = lista.get(i);

                            try {
                                mUsbThermalPrinter.reset();
                                mUsbThermalPrinter.setAlgin(UsbThermalPrinter.ALGIN_LEFT);

                                String msg =
                                        "Serie: " + dataBoleto.getNumFolio() +
                                        " $" + dataBoleto.getValorTarifa() + " " +
                                        "Fecha/Hora: " + dataBoleto.getFecha() + "\n" +
                                        "===========================" + "\n";

                                mUsbThermalPrinter.addString(msg);
                                mUsbThermalPrinter.printString();
                                mUsbThermalPrinter.walkPaper(2);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Result = e.toString();
                                if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                                    nopaper = true;
                                } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                                    handler.sendMessage(handler.obtainMessage(OVERHEAT, 1, 0, null));
                                } else {
                                    handler.sendMessage(handler.obtainMessage(PRINTERR, 1, 0, null));
                                }
                            } finally {
                                handler.sendMessage(handler.obtainMessage(CANCELPROMPT, 1, 0, null));
                                if (nopaper) {
                                    handler.sendMessage(handler.obtainMessage(NOPAPER, 1, 0, null));
                                    nopaper = false;

                                }
                            }

                        }

                        try {
                            mUsbThermalPrinter.reset();
                            mUsbThermalPrinter.setAlgin(UsbThermalPrinter.ALGIN_LEFT);

                            String msg =
                                    "Total Ventas: $" + ApplicationContext.getInstance().getValorTotal() +"\n" +
                                            "===========================" + "\n";

                            mUsbThermalPrinter.addString(msg);
                            mUsbThermalPrinter.printString();
                            mUsbThermalPrinter.walkPaper(2);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Result = e.toString();
                            if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                                nopaper = true;
                            } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                                handler.sendMessage(handler.obtainMessage(OVERHEAT, 1, 0, null));
                            } else {
                                handler.sendMessage(handler.obtainMessage(PRINTERR, 1, 0, null));
                            }
                        } finally {
                            handler.sendMessage(handler.obtainMessage(CANCELPROMPT, 1, 0, null));
                            if (nopaper) {
                                handler.sendMessage(handler.obtainMessage(NOPAPER, 1, 0, null));
                                nopaper = false;

                            }
                        }

                    } else {
                        MSG = "Su dispositivo se encuentra Sincronizado con el servidor..";
                    }


                } else {
                    // pdCarga.dismiss();
                    MSG = "Problemas de conexion, verifique y vuelva intentar..";

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pdCarga.dismiss();

        }

    }


    public void onBackPressed() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botï¿½n al pulsar ir a atrï¿½s

            startActivity(new Intent(this, SelectFechaHora.class));
            finish();

            return true;
        } else if (keyCode == KeyEvent.KEYCODE_HOME
                && event.getRepeatCount() == 0) {

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}

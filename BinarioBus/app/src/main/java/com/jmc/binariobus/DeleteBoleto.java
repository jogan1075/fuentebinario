package com.jmc.binariobus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import DAO.BoletosDAO;
import DAO.TarifasDAO;
import DAO.UsuarioDAO;
import MOD.BoletoMOD;
import MOD.TarifasMOD;
import Utils.FontFactory;
import Utils.SharedPreferencesUtils;
import Utils.Utilidades;
import adapter.AdapterBoleto;

import static Utils.Utilidades.isOnline;

public class DeleteBoleto extends AppCompatActivity {

    EditText editIdBoleto;
    ListView lista;
    BoletosDAO _tarifasDAO;
    List<BoletoMOD> list;
    AdapterBoleto adapter;
    BoletoMOD data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_boleto);

        editIdBoleto = (EditText) findViewById(R.id.editText);

        Button button = (Button) findViewById(R.id.button);

        editIdBoleto.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        button.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        lista = (ListView) findViewById(R.id.listView);

        _tarifasDAO = BoletosDAO.getInstance(this);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                data = (BoletoMOD) parent.getItemAtPosition(position);
                if (data != null) {





//                    if (Utilidades.isOnline(getApplicationContext())) {
                        Boolean resp = _tarifasDAO.UpdateEstadoBoletoByIDLocal(String.valueOf(data.getNumFolio()));

                        if (resp) {
                            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(DeleteBoleto.this);
                            dialogo1.setTitle("Eliminacion Boleto");
                            dialogo1.setMessage("Eliminacion del boleto existosa, ¿Desea continuar? Presione No para Salir.");
                            dialogo1.setCancelable(false);
                            dialogo1.setPositiveButton("SI",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialogo1, int id) {
                                            lista.setAdapter(null);
                                            editIdBoleto.setText("");
                                        }
                                    });
                            dialogo1.setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialogo1, int id) {
                                            startActivity(new Intent(getApplicationContext(), MenuPrincipal.class));
                                            finish();
                                        }
                                    });
                            dialogo1.show();
                        } else {
                            Utilidades.dialogErrorSimple(DeleteBoleto.this, getResources().getString(R.string.title_msg_error_inesperado), getResources().getString(R.string.msg_error_inesperado));
                        }
//                    } else {
//                        Utilidades.dialogErrorSimple(DeleteBoleto.this, getResources().getString(R.string.title_msg_sin_conexion), getResources().getString(R.string.msg_sin_conexion));
//                    }
                }

            }
        });
    }

    public void btnBuscarBoleto(View v) {
        new LoadData().execute();
    }

    public void onBackPressed() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botï¿½n al pulsar ir a atrï¿½s

            startActivity(new Intent(this, MenuPrincipal.class));
            finish();

            return true;
        } else if (keyCode == KeyEvent.KEYCODE_HOME
                && event.getRepeatCount() == 0) {

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    public class LoadData extends AsyncTask<String, Void, String> {
        ProgressDialog pdCarga;
        Boolean edittextBool = false;
        Boolean onlineBool = false;
        String MSG = "";

        UsuarioDAO _usuarioDAO;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pdCarga = new ProgressDialog(DeleteBoleto.this);
            pdCarga.setTitle("Procesando...");
            pdCarga.setMessage("Por favor espere...");
            pdCarga.setCancelable(false);
            pdCarga.setIndeterminate(true);
            pdCarga.show();

//            _usuarioDAO = UsuarioDAO.getInstance(getApplicationContext());
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub


            try {

                if (!editIdBoleto.getText().toString().isEmpty()) {

                        list = _tarifasDAO.getBoletosByIdLocal(editIdBoleto.getText().toString());

                } else {
                    lista.setAdapter(null);
                    edittextBool = true;
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pdCarga.dismiss();

            if (list != null) {
                adapter = new AdapterBoleto(DeleteBoleto.this, R.layout.item_boleto,
                        list);

                lista.setAdapter(adapter);
            } else if (onlineBool) {
                Utilidades.dialogErrorSimple(DeleteBoleto.this, getResources().getString(R.string.title_msg_sin_conexion), getResources().getString(R.string.msg_sin_conexion));
            } else if (edittextBool) {

                Utilidades.dialogErrorSimple(DeleteBoleto.this, getResources().getString(R.string.title_msg_errorDeleteBoleto_emptyText), getResources().getString(R.string.msg_errorDeleteBoleto_emptyText));
            }

        }

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}

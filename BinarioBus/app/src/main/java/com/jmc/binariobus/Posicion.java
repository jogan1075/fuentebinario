package com.jmc.binariobus;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by jmunoz on 29-10-17.
 */

public class Posicion extends Service implements LocationListener {
    final static String TAG = "MDM GPS SRV";

    boolean locationOn = false;
    int timeUpdate = 1000 * 60 * 5;
    long timeRefreshPost = 1000 * 60 * 5;
    float distanceUpdate = 1000;

    LocationManager locationManager;
    private final Handler handler = new Handler();

    public class MyBinder extends Binder {
        Posicion getService() {
            return Posicion.this;
        }
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return new MyBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        locationManager.removeUpdates(this);
        super.onDestroy();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        handler.post(getData);
        super.onStart(intent, startId);
    }

    private final Runnable getData = new Runnable() {
        public void run() {
            Log.d(TAG, "getData");
            getDataFrame();
        }
    };

    private void getDataFrame() {
        Log.d(TAG, "getDataFrame");
        if (locationOn) {
            locationOn = false;
            stopGPS();
        }
        if (!locationOn) {
            startGPS();
            locationOn = true;
        }

        handler.postDelayed(getData, timeRefreshPost);
    }

    class ParamsLocation {
        int idUsuario;
        String provider;
        int lat;
        int lon;

        public ParamsLocation(int idUsuario, String provider, int lat, int lon) {
            this.lat = lat;
            this.lon = lon;

            this.provider = provider;
            this.idUsuario = idUsuario;
        }
    }

    public void onLocationChanged(Location location) {
        // Se guarda en la sesion en curso
      ApplicationContext.getInstance().setLatutid(location.getLatitude());
        ApplicationContext.getInstance().setLongitud(location
                .getLongitude());



//        if (null != location && null != AplicacionEmergencias.session
//                && AplicacionEmergencias.session.getIdUsuario() != 0) {
//            Log.d(TAG, "Nueva ubicación, usuario logeado => enviar");
//
////            int idUsuario = AplicacionEmergencias.session.getIdUsuario();

            Log.e(TAG, String.format("%d %d",
                    (int) (location.getLatitude() * 1E6),
                    (int) (location.getLongitude() * 1E6)));

//			new SendLocation().execute(new ParamsLocation(idUsuario, location
//					.getProvider(), (int) (location.getLatitude() * 1E6),
//					(int) (location.getLongitude() * 1E6)));
//        } else {
//            Log.d(TAG, "Nueva ubicación, sin usuario logeado => no enviada");
//        }

    }

    public void onProviderDisabled(String provider) {
        Log.e(TAG, "onProviderDisabled: " + provider);
    }

    public void onProviderEnabled(String provider) {
        Log.e(TAG, "onProviderEnabled: " + provider);
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.e(TAG, "onStatusChanged: " + provider);
    }

    public void startGPS() {
        Log.d(TAG, "Iniciando búsqueda de ubicación por GPS y RED...");

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, timeUpdate, distanceUpdate,
                this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                timeUpdate, distanceUpdate, this);

        Location location = locationManager
                .getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (location != null) {
            Log.d(TAG,
                    "Última coordenada GPS conocida = " + location.toString());

            this.onLocationChanged(location);
        } else {
            location = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                Log.d(TAG,
                        "Última coordenada RED conocida = "
                                + location.toString());

                this.onLocationChanged(location);
            }
        }
    }

    public void stopGPS() {
        Log.d(TAG, "Deteniendo búsqueda de ubicación...");

        locationManager.removeUpdates(this);
    }

//	private class SendLocation extends AsyncTask<ParamsLocation, Void, Void> {
//		@Override
//		protected Void doInBackground(ParamsLocation... params) {
//			// agrego la posición al sistema
//			List<BasicNameValuePair> datos = new ArrayList<BasicNameValuePair>();
//
//			datos.add(new BasicNameValuePair("action", "saveposition"));
//			datos.add(new BasicNameValuePair("usuario", String
//					.valueOf(params[0].idUsuario)));
//			datos.add(new BasicNameValuePair("lat", String
//					.valueOf(params[0].lat)));
//			datos.add(new BasicNameValuePair("lon", String
//					.valueOf(params[0].lon)));
//
////			Connect conn = new Connect();
////			String resu = conn.sendPosition(datos);
//
//			Log.e("ENVIO de ubicacion --- resultado : ", resu);
//
//			return null;
//		}
//	}
}

package com.jmc.binariobus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ListView;

import java.util.List;

import DAO.BoletosDAO;
import MOD.BoletoMOD;
import Utils.SharedPreferencesUtils;
import adapter.AdapterDesdeHasta;
import adapter.AdapterTarifas;

public class ListadoBoletosVendidos extends AppCompatActivity {
ListView listView;
    List<BoletoMOD> lista;
    BoletosDAO _boletosDAO;
    AdapterDesdeHasta adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_boletos_vendidos);
        _boletosDAO = BoletosDAO.getInstance(ApplicationContext.getContext());
        listView = (ListView) findViewById(R.id.reciclador);
        lista = _boletosDAO.BusquedaBoletosDesdeHasta(ApplicationContext.getInstance().getDates());
        adapter = new AdapterDesdeHasta(this, R.layout.item_tarifa,
                lista);

        listView.setAdapter(adapter);
    }

    public void onBackPressed() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botï¿½n al pulsar ir a atrï¿½s

            startActivity(new Intent(this, DesdeHasta.class));
            finish();

            return true;
        } else if (keyCode == KeyEvent.KEYCODE_HOME
                && event.getRepeatCount() == 0) {

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}

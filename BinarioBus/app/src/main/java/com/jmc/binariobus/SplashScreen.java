package com.jmc.binariobus;

import android.app.Activity;
import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

import DAO.ConfigDAO;
import MOD.ConfigMOD;

public class SplashScreen extends AppCompatActivity {
    private static final long SPLASH_SCREEN_DELAY = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

//        startActivity(new Intent(this,Main_Activity.class ));
//        startActivity(new Intent(this,Login.class ));
//        finish();

        ConfigDAO _configDAO= ConfigDAO.getInstance(this);
        ConfigMOD _configMOD = _configDAO.getConfigLocal();
        if(_configMOD == null){

            _configDAO.InsertarConfig(1, "binariobus.cl", "binariob_binario", "binariob_randami", "Binario123**");
        }



        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                // Start the next activity
                Intent mainIntent = new Intent().setClass(
                        SplashScreen.this, Login.class);
                startActivity(mainIntent);

                // Close the activity so the user won't able to go back this
                // activity pressing Back button
                finish();
            }
        };

        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);

    }
}

package adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jmc.binariobus.ApplicationContext;
import com.jmc.binariobus.R;

import java.util.ArrayList;
import java.util.List;

import DAO.BoletosDAO;
import DAO.TarifasDAO;
import MOD.BoletoMOD;
import MOD.TarifasMOD;
import Utils.FontFactory;

/**
 * Created by jogan1075 on 27-03-17.
 */

public class AdapterDesdeHasta extends ArrayAdapter<BoletoMOD> {

    List<BoletoMOD> list;

    BoletosDAO _tarifaDAO;
    TarifasDAO tarifasDAO;

    BoletoMOD _taridaMOD;
    private Context activity;
    private final int[] bgColors = new int[]{R.color.list_bg_1,
            R.color.list_bg_2};

    public AdapterDesdeHasta(Context context, int resource,
                             List<BoletoMOD> objects) {
        super(context, resource, objects);
        this.list = new ArrayList<BoletoMOD>();
        list.addAll(objects);

        this.activity = context;
        _tarifaDAO = BoletosDAO.getInstance(activity);
        tarifasDAO= TarifasDAO.getInstance(activity);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public BoletoMOD getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            // inflate the layout
            LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_desdehasta, parent,
                    false);
            if (position % 2 == 0) {
                convertView
                        .setBackgroundResource(R.drawable.round_border_item_list);
            } else {
                convertView
                        .setBackgroundResource(R.drawable.round_border_item_list2);
            }
        }

        BoletoMOD objectItem = list.get(position);

        if (objectItem != null) {

            TextView txtFolio = (TextView) convertView
                    .findViewById(R.id.txtFolio);
            TextView txtDescripcion = (TextView) convertView
                    .findViewById(R.id.textView8);

            TextView txtvalor = (TextView) convertView
                    .findViewById(R.id.textView9);

            TextView txtfecha = (TextView) convertView
                    .findViewById(R.id.textView10);


            txtFolio.setText("Folio #"+objectItem.getNumFolio().toString());
            txtfecha.setText(objectItem.getFecha().toString());
            txtvalor.setText("$"+objectItem.getValorTarifa().toString());
            txtvalor.setTextColor(Color.BLACK);
            txtDescripcion.setTextColor(Color.BLACK);
            txtDescripcion.setText(tarifasDAO.getNameTarifasByIdLocal(objectItem.getNombreTarifa().toString()));

            txtFolio.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
            txtfecha.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
            txtvalor.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
            txtDescripcion.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        }

        return convertView;
    }
}

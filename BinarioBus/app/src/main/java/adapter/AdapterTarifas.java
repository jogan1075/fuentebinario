package adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jmc.binariobus.ApplicationContext;
import com.jmc.binariobus.R;

import java.util.ArrayList;
import java.util.List;

import DAO.TarifasDAO;
import MOD.TarifasMOD;
import Utils.FontFactory;

/**
 * Created by jogan1075 on 27-03-17.
 */

public class AdapterTarifas extends ArrayAdapter<TarifasMOD> {

    List<TarifasMOD> list;

    TarifasDAO _tarifaDAO;
    TarifasMOD _taridaMOD;
    private Context activity;
    private final int[] bgColors = new int[] { R.color.list_bg_1,
            R.color.list_bg_2 };

    public AdapterTarifas(Context context, int resource,
                               List<TarifasMOD> objects) {
        super(context, resource, objects);
        this.list = new ArrayList<TarifasMOD>();
        list.addAll(objects);

        this.activity = context;
        _tarifaDAO = TarifasDAO.getInstance(activity);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public TarifasMOD getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            // inflate the layout
            LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_tarifa, parent,
                    false);
			if (position % 2 == 0) {
				convertView
						.setBackgroundResource(R.drawable.round_border_item_list);
			} else {
				convertView
						.setBackgroundResource(R.drawable.round_border_item_list2);
			}
        }

        // object item based on the position
        TarifasMOD objectItem = list.get(position);

        if (objectItem != null) {
            // get the TextView and then set the text (item name) and tag (item
            // ID)
            // values
            TextView txtDescripcion = (TextView) convertView
                    .findViewById(R.id.textView3);
            TextView txtvalor = (TextView) convertView
                    .findViewById(R.id.textView6);

            txtvalor.setText(objectItem.getValor());
            txtvalor.setTextColor(Color.BLACK);
            txtDescripcion.setTextColor(Color.BLACK);
            txtDescripcion.setText(objectItem.getNombreTarifa());

            txtvalor.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
            txtDescripcion.setTypeface(FontFactory.getInstance().getNormalFont(ApplicationContext.getContext()));
        }

        return convertView;
    }
}

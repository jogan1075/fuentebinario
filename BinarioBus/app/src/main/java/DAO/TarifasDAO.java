package DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import MOD.BoletoMOD;
import MOD.ConfigMOD;
import MOD.TarifasMOD;
import MOD.UsuarioMOD;
import Utils.ConnectMySQL;
import Utils.DataHelper;

/**
 * Created by jogan1075 on 27-03-17.
 */

public class TarifasDAO {

    private static TarifasDAO instance;
    ConnectMySQL mysql;
    private Connection connect = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private SQLiteDatabase database;
    ConfigDAO _configDAO;
    ConfigMOD _configMOD;

    public static TarifasDAO getInstance(Context context) {
        if (instance == null) {
            instance = new TarifasDAO(context);
        }
        return instance;
    }

    private TarifasDAO(Context context) {
        mysql = new ConnectMySQL();
        DataHelper _DataHelper = DataHelper.getInstance(context);
        database = _DataHelper.getWritableDatabase();
        _configDAO = ConfigDAO.getInstance(context);
    }


    public void insert(ContentValues values) {

        try {
            database.insert(TarifasMOD.NOMBRE_TABLE_TARIFAS, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<TarifasMOD> getTarifasByIdVendedorLocal(String IdVendedor) {
        List<TarifasMOD> results = new ArrayList<TarifasMOD>();
        TarifasMOD tarifasMOD = null;

        Cursor c = database.rawQuery("select * from TARIFAS where Tipo_Bus = '" + IdVendedor + "';", null);
        if (c != null && c.moveToFirst()) {
            while (c.moveToNext()) {
                tarifasMOD = new TarifasMOD();
                tarifasMOD.setId(c.getInt(c.getColumnIndex("Tipo_Id")));
                tarifasMOD.setNombreTarifa(c.getString(c.getColumnIndex("Tipo_Nombre")));
                tarifasMOD.setValor(c.getString(c.getColumnIndex("Tipo_Valor")));
                tarifasMOD.setAccion(c.getString(c.getColumnIndex("Tipo_Accion")));
                tarifasMOD.setBus(c.getString(c.getColumnIndex("Tipo_Bus")));

                results.add(tarifasMOD);
            }
        }
        return results;
    }



    public String getNameTarifasByIdLocal(String ID) {
        String nombre = null;

        Cursor c = database.rawQuery("select Tipo_Nombre from TARIFAS where Tipo_Id = '" + ID + "';", null);
        if (c != null && c.moveToFirst()) {

               nombre =c.getString(0);

        }
        return nombre;
    }

    public void getTarifasbyIdVendedor(String IdVendedor) {
//        List<TarifasMOD> results = new ArrayList<TarifasMOD>();
        TarifasMOD _tarifaMOD;
        String path;
        String query = "select * from tipos_de_tarifas where tip_bus='" + IdVendedor + "';";

        try {
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            Class.forName("com.mysql.jdbc.Driver");

            _configMOD = _configDAO.getConfigLocal();
            if (_configMOD != null) {
                path = "jdbc:mysql://" + _configMOD.getHost() + "/"
                        + _configMOD.getNombreBD() + "?" + "user="
                        + _configMOD.getUserBD() + "&password="
                        + _configMOD.getPassBD();
            }else{
                path = "jdbc:mysql://" + ConnectMySQL.MYSQL_IP + "/"
                        + ConnectMySQL.MYSQL_DBNAME + "?" + "user="
                        + ConnectMySQL.MYSQL_USERNAME + "&password="
                        + ConnectMySQL.MYSQL_PASSWORD;

            }

            connect = (Connection) DriverManager.getConnection(path);
            statement = (Statement) connect.createStatement();
            resultSet = (ResultSet) statement.executeQuery(query);
            while (resultSet.next()) {
                _tarifaMOD = new TarifasMOD();
                _tarifaMOD.setId(resultSet.getInt("tipo_id"));
                _tarifaMOD.setNombreTarifa(resultSet.getString("tipo_nombre"));
                _tarifaMOD.setValor(resultSet.getString("tip_valor"));
                _tarifaMOD.setAccion(resultSet.getString("tip_accion"));
                _tarifaMOD.setBus(resultSet.getString("tip_bus"));

                insert(_tarifaMOD.ValoresTarifas(_tarifaMOD.getId(), _tarifaMOD.getNombreTarifa(), _tarifaMOD.getValor(), _tarifaMOD.getAccion(), _tarifaMOD.getBus()));
//                results.add(_tarifaMOD);
            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

//        return results;
    }





}

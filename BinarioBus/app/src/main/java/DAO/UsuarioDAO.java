package DAO;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import com.mysql.jdbc.Connection;

import com.mysql.jdbc.Statement;


import MOD.ConfigMOD;
import MOD.UsuarioMOD;
import Utils.ConnectMySQL;
import Utils.SharedPreferencesUtils;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;
import android.util.Log;

public class UsuarioDAO {

    private static UsuarioDAO instance;
    ConnectMySQL mysql;
    private Connection connect = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    TarifasDAO _tarifasDAO;
    ConfigDAO _configDAO;
    ConfigMOD _configMOD;

    public static UsuarioDAO getInstance(Context context) {
        if (instance == null) {
            instance = new UsuarioDAO(context);
        }
        return instance;
    }

    private UsuarioDAO(Context context) {
        mysql = new ConnectMySQL();
        _tarifasDAO = TarifasDAO.getInstance(context);
        _configDAO = ConfigDAO.getInstance(context);
    }

    public Boolean LoginUsuario(String correo, String password) {
        ArrayList<UsuarioMOD> results = new ArrayList<UsuarioMOD>();
        Boolean resp = false;
        UsuarioMOD obj = null;
        String path;
        try {
            String script = "SELECT cho_id,cho_url_logo,cho_nombre,cho_apellido,cho_nro_bus,max(CAST(bo_num_folio AS INT)) as ultimoBoletoVendido, cho_politica_seguridad\n" +
                    "FROM vendedores inner join boletos on vendedores.cho_id = bo_usuario\n" +
                    "where cho_email='"+correo+"' \n" +
                    "and cho_constrasena ='"+password+"';";

            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            Class.forName("com.mysql.jdbc.Driver");

            _configMOD = _configDAO.getConfigLocal();
            if (_configMOD != null) {
                path = "jdbc:mysql://" + _configMOD.getHost() + "/"
                        + _configMOD.getNombreBD() + "?" + "user="
                        + _configMOD.getUserBD() + "&password="
                        + _configMOD.getPassBD();
            }else{
                path = "jdbc:mysql://" + ConnectMySQL.MYSQL_IP + "/"
                        + ConnectMySQL.MYSQL_DBNAME + "?" + "user="
                        + ConnectMySQL.MYSQL_USERNAME + "&password="
                        + ConnectMySQL.MYSQL_PASSWORD;

            }

            connect = (Connection) DriverManager.getConnection(path);
            statement = (Statement) connect.createStatement();
            resultSet = (ResultSet) statement.executeQuery(script);

            while (resultSet.next()) {
                obj = new UsuarioMOD();
                obj.setId(resultSet.getInt("cho_id"));
                obj.setLogo(resultSet.getString("cho_url_logo"));
                obj.setNombre(resultSet.getString("cho_nombre") + " " + resultSet.getString("cho_apellido"));
                obj.setNroBus(resultSet.getString("cho_nro_bus"));
                obj.setUltimoBoletoVendido(resultSet.getString("ultimoBoletoVendido"));
                obj.setPoliticaSeguridad(resultSet.getString("cho_politica_seguridad"));
                results.add(obj);
            }

            _tarifasDAO.getTarifasbyIdVendedor(String.valueOf(obj.getId()));

            SharedPreferencesUtils.savePreference("IdUsuario", String.valueOf(obj.getId()));
            SharedPreferencesUtils.savePreference("Logo", obj.getLogo());
            SharedPreferencesUtils.savePreference("NroBus", obj.getNroBus());
            SharedPreferencesUtils.savePreference("NombreCompleto", obj.getNombre());
            SharedPreferencesUtils.savePreference("ultimoBoletoVendido", obj.getUltimoBoletoVendido());
            SharedPreferencesUtils.savePreference("politicaSeguridad",obj.getPoliticaSeguridad());

            if (results.size() > 0) {
                resp = true;

            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return resp;
    }


}

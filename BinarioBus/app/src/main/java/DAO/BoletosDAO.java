package DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import MOD.BoletoMOD;
import MOD.ConfigMOD;
import MOD.TarifasMOD;
import Utils.ConnectMySQL;
import Utils.DataHelper;
import Utils.SharedPreferencesUtils;

/**
 * Created by jmunoz on 28-10-17.
 */

public class BoletosDAO {

    private static BoletosDAO instance;
    ConnectMySQL mysql;
    private Connection connect = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private SQLiteDatabase database;
    ConfigDAO _configDAO;
    ConfigMOD _configMOD;

    public static BoletosDAO getInstance(Context context) {
        if (instance == null) {
            instance = new BoletosDAO(context);
        }
        return instance;
    }

    private BoletosDAO(Context context) {
        mysql = new ConnectMySQL();
        DataHelper _DataHelper = DataHelper.getInstance(context);
        database = _DataHelper.getWritableDatabase();
        _configDAO = ConfigDAO.getInstance(context);
    }

    public Boolean insert(String usuario, String valorTarifa, String fechaHora, int idTarifa,
                          String longitud, String latitud, String ultimoBoletoVendido) {

        Boolean resp = false;
        try {
            long x = database.insert(BoletoMOD.NOMBRE_TABLE_BOLETOS, null, BoletoMOD.ValoresTarifas(usuario, valorTarifa,
                    fechaHora, idTarifa + "", latitud, longitud, "0", "0", ultimoBoletoVendido));

            if (x > 0) {

                SharedPreferencesUtils.savePreference("ultimoBoletoVendido", "0" + ultimoBoletoVendido);
                ultimoBoletoVendido.toString();

                resp = true;
            } else {
                resp = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }


    public Boolean InsertarBoleto(String usuario, String valorTarifa, String fechaHora, int idTarifa,
                                  String longitud, String latitud, String ultimoBoletoVendido,String eliminado) {
        int resp;
        Boolean retorno = false;
        String path;
        BoletoMOD _boletoMOD = null;
        String sql = "insert into boletos (bo_usuario, bo_tarifa, bo_tiempo, bo_nombre_tarifa, bo_latitud, bo_longitud, bo_verificado, bo_eliminado,bo_num_folio)" +
                " " +
                "VALUES ('" + usuario + "','" + valorTarifa + "','" + fechaHora + "','" + idTarifa + "','" + latitud + "','" + longitud + "','" + 0 + "','" + eliminado + "','" + ultimoBoletoVendido + "')";

        try {
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            Class.forName("com.mysql.jdbc.Driver");

            _configMOD = _configDAO.getConfigLocal();
            if (_configMOD != null) {
                path = "jdbc:mysql://" + _configMOD.getHost() + "/"
                        + _configMOD.getNombreBD() + "?" + "user="
                        + _configMOD.getUserBD() + "&password="
                        + _configMOD.getPassBD();
            } else {
                path = "jdbc:mysql://" + ConnectMySQL.MYSQL_IP + "/"
                        + ConnectMySQL.MYSQL_DBNAME + "?" + "user="
                        + ConnectMySQL.MYSQL_USERNAME + "&password="
                        + ConnectMySQL.MYSQL_PASSWORD;

            }

            connect = (Connection) DriverManager.getConnection(path);
            statement = (Statement) connect.createStatement();
            resp = statement.executeUpdate(sql);
//            resultSet = (ResultSet) statement.executeQuery(sql);
            if (resp > 0) {
                retorno = true;


            } else {
                retorno = false;
            }


        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return retorno;
    }


    public Boolean UpdateEstadoBoletoByID(String IdBoleto) {
        int resp;
        Boolean retorno = false;
        String path;

        String sql = "update boletos set bo_eliminado = '1' where bo_num_folio='" + IdBoleto + "';";

        try {
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            Class.forName("com.mysql.jdbc.Driver");


            _configMOD = _configDAO.getConfigLocal();
            if (_configMOD != null) {
                path = "jdbc:mysql://" + _configMOD.getHost() + "/"
                        + _configMOD.getNombreBD() + "?" + "user="
                        + _configMOD.getUserBD() + "&password="
                        + _configMOD.getPassBD();
            } else {
                path = "jdbc:mysql://" + ConnectMySQL.MYSQL_IP + "/"
                        + ConnectMySQL.MYSQL_DBNAME + "?" + "user="
                        + ConnectMySQL.MYSQL_USERNAME + "&password="
                        + ConnectMySQL.MYSQL_PASSWORD;

            }

            connect = (Connection) DriverManager.getConnection(path);
            statement = (Statement) connect.createStatement();
            resp = statement.executeUpdate(sql);

            if (resp > 0) {
                retorno = true;
            }

        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return retorno;
    }

    public Boolean UpdateEstadoBoletoByIDLocal(String IdBoleto) {
        int resp;
        Boolean retorno = false;
        String path;

//        String sql = "update boletos set bo_eliminado = '1' where bo_num_folio='" + IdBoleto + "';";


        ContentValues values = new ContentValues();
        values.put("Eliminado", "1");
        long x = database.update("BOLETOS", values, "NumFolio" + "=" + IdBoleto, null);

        if (x == 1) {
            retorno = true;


        }
        return retorno;
    }


    public List<BoletoMOD> getBoletoById(String IdBoleto) {
        List<BoletoMOD> results = new ArrayList<BoletoMOD>();
        BoletoMOD _boletoMOD;
        String path;
        String query = "select bo.bo_id,bo.bo_usuario, bo.bo_tarifa,bo.bo_tiempo, ta.tipo_nombre,\n" +
                "bo.bo_latitud,bo.bo_longitud,bo.bo_verificado,bo.bo_eliminado,bo.bo_num_folio \n" +
                "from boletos as bo\n" +
                "inner join \n" +
                "tipos_de_tarifas as ta \n" +
                "on ta.tipo_id = bo.bo_nombre_tarifa \n" +
                "where bo.bo_num_folio = '" + IdBoleto + "'  and bo.bo_eliminado != '1';";

        try {
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            Class.forName("com.mysql.jdbc.Driver");

            _configMOD = _configDAO.getConfigLocal();
            if (_configMOD != null) {
                path = "jdbc:mysql://" + _configMOD.getHost() + "/"
                        + _configMOD.getNombreBD() + "?" + "user="
                        + _configMOD.getUserBD() + "&password="
                        + _configMOD.getPassBD();
            } else {
                path = "jdbc:mysql://" + ConnectMySQL.MYSQL_IP + "/"
                        + ConnectMySQL.MYSQL_DBNAME + "?" + "user="
                        + ConnectMySQL.MYSQL_USERNAME + "&password="
                        + ConnectMySQL.MYSQL_PASSWORD;

            }

            connect = (Connection) DriverManager.getConnection(path);
            statement = (Statement) connect.createStatement();
            resultSet = (ResultSet) statement.executeQuery(query);
            while (resultSet.next()) {
                _boletoMOD = new BoletoMOD();
                _boletoMOD.setIdBoleto(resultSet.getInt("bo_id"));
                _boletoMOD.setIdUsuario(resultSet.getString("bo_usuario"));
                _boletoMOD.setValorTarifa(resultSet.getString("bo_tarifa"));
                _boletoMOD.setFecha(resultSet.getString("bo_tiempo"));
                _boletoMOD.setNombreTarifa(resultSet.getString("tipo_nombre"));
                _boletoMOD.setLatitud(resultSet.getString("bo_latitud"));
                _boletoMOD.setLongitud(resultSet.getString("bo_longitud"));
                _boletoMOD.setVerificado(resultSet.getInt("bo_verificado"));
                _boletoMOD.setEliminado(resultSet.getInt("bo_eliminado"));
                _boletoMOD.setNumFolio(resultSet.getString("bo_num_folio"));

                results.add(_boletoMOD);
            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return results;
    }

    public List<BoletoMOD> getBoletosByIdLocal(String folio) {
        BoletoMOD boletoMOD = null;
        List<BoletoMOD> results = new ArrayList<BoletoMOD>();

        Cursor c = database.rawQuery("select bo.Id,bo.IdUsuario, bo.Valor,bo.Fecha, ta.Tipo_Nombre," +
                "bo.Latitud,bo.Longitud,bo.Verificado,bo.Eliminado,bo.NumFolio " +
                "from BOLETOS as bo inner join TARIFAS as ta on ta.Tipo_Id = bo.IdNombreTarifa where bo.Eliminado != 1 and  bo.NumFolio = " + folio , null);

        if (c != null && c.moveToFirst()) {



                boletoMOD = new BoletoMOD();
                boletoMOD.setIdBoleto(c.getInt(c.getColumnIndex("Id")));
                boletoMOD.setIdUsuario(c.getString(c.getColumnIndex("IdUsuario")));
                boletoMOD.setValorTarifa(c.getString(c.getColumnIndex("Valor")));
                boletoMOD.setFecha(c.getString(c.getColumnIndex("Fecha")));
                boletoMOD.setNombreTarifa(c.getString(c.getColumnIndex("Tipo_Nombre")));
                boletoMOD.setLatitud(c.getString(c.getColumnIndex("Latitud")));
                boletoMOD.setLongitud(c.getString(c.getColumnIndex("Longitud")));
                boletoMOD.setVerificado(c.getInt(c.getColumnIndex("Verificado")));
                boletoMOD.setEliminado(c.getInt(c.getColumnIndex("Eliminado")));
                boletoMOD.setNumFolio(c.getString(c.getColumnIndex("NumFolio")));
                results.add(boletoMOD);

        }

        return results;
    }

    public List<BoletoMOD> getAllBoletosLocales() {
        BoletoMOD boletoMOD = null;
        List<BoletoMOD> results = new ArrayList<BoletoMOD>();
        Cursor c = database.rawQuery("select * from BOLETOS", null);

        if (c != null && c.moveToFirst()) {

            while (c.moveToNext()) {

                boletoMOD = new BoletoMOD();
                boletoMOD.setIdBoleto(c.getInt(c.getColumnIndex("Id")));
                boletoMOD.setIdUsuario(c.getString(c.getColumnIndex("IdUsuario")));
                boletoMOD.setValorTarifa(c.getString(c.getColumnIndex("Valor")));
                boletoMOD.setFecha(c.getString(c.getColumnIndex("Fecha")));
                boletoMOD.setNombreTarifa(c.getString(c.getColumnIndex("IdNombreTarifa")));
                boletoMOD.setLatitud(c.getString(c.getColumnIndex("Latitud")));
                boletoMOD.setLongitud(c.getString(c.getColumnIndex("Longitud")));
                boletoMOD.setVerificado(c.getInt(c.getColumnIndex("Verificado")));
                boletoMOD.setEliminado(c.getInt(c.getColumnIndex("Eliminado")));
                boletoMOD.setNumFolio(c.getString(c.getColumnIndex("NumFolio")));
                results.add(boletoMOD);
            }
        }

        return results;
    }


    public List<BoletoMOD> BusquedaBoletosDesdeHasta(List<String> dates) {
        BoletoMOD boletoMOD = null;
        List<BoletoMOD> results = new ArrayList<BoletoMOD>();

        String path;
        String query = "SELECT *FROM boletos WHERE bo_usuario = 2 and bo_tiempo BETWEEN '" + dates.get(0).toString() + "' AND '" + dates.get(1).toString() + "' \n";

        try {
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            Class.forName("com.mysql.jdbc.Driver");

            _configMOD = _configDAO.getConfigLocal();
            if (_configMOD != null) {
                path = "jdbc:mysql://" + _configMOD.getHost() + "/"
                        + _configMOD.getNombreBD() + "?" + "user="
                        + _configMOD.getUserBD() + "&password="
                        + _configMOD.getPassBD();
            } else {
                path = "jdbc:mysql://" + ConnectMySQL.MYSQL_IP + "/"
                        + ConnectMySQL.MYSQL_DBNAME + "?" + "user="
                        + ConnectMySQL.MYSQL_USERNAME + "&password="
                        + ConnectMySQL.MYSQL_PASSWORD;

            }
            connect = (Connection) DriverManager.getConnection(path);
            statement = (Statement) connect.createStatement();
            resultSet = (ResultSet) statement.executeQuery(query);
            while (resultSet.next()) {
                boletoMOD = new BoletoMOD();
                boletoMOD.setIdBoleto(resultSet.getInt("bo_id"));
                boletoMOD.setIdUsuario(resultSet.getString("bo_usuario"));
                boletoMOD.setValorTarifa(resultSet.getString("bo_tarifa"));
                boletoMOD.setFecha(resultSet.getString("bo_tiempo"));
                boletoMOD.setNombreTarifa(resultSet.getString("bo_nombre_tarifa"));
                boletoMOD.setLatitud(resultSet.getString("bo_latitud"));
                boletoMOD.setLongitud(resultSet.getString("bo_longitud"));
                boletoMOD.setVerificado(resultSet.getInt("bo_verificado"));
                boletoMOD.setEliminado(resultSet.getInt("bo_eliminado"));
                boletoMOD.setNumFolio(resultSet.getString("bo_num_folio"));
                results.add(boletoMOD);
            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return results;
    }


    public void deleteBoletoByID(int id) {

        database.execSQL("delete from BOLETOS where Id = '" + id + "'");
    }

}

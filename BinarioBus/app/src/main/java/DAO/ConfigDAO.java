package DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import java.sql.ResultSet;

import MOD.ConfigMOD;
import Utils.DataHelper;

/**
 * Created by jogan1075 on 24-10-17.
 */

public class ConfigDAO {

    private static ConfigDAO instance;
    // ConnectMySQL mysql;
    private Connection connect = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private SQLiteDatabase database;

    public static ConfigDAO getInstance(Context context) {
        if (instance == null) {
            instance = new ConfigDAO(context);
        }
        return instance;
    }

    private ConfigDAO(Context context) {
        DataHelper _DataHelper = DataHelper.getInstance(context);
        database = _DataHelper.getWritableDatabase();
        // mysql = new ConnectMySQL();
    }

    public boolean InsertarConfig(int Id, String host, String nombre, String user,
                                  String pass) {

        long x = database.insert("CONFIGURACION", null,
                ConfigMOD.ValoresCONFIG(Id, host, nombre, user, pass));
        if (x == -1) {
            Log.d("JMC", "no insertado");
            return false;
        } else {
            return true;
        }
    }

    public Boolean ActualizarConfig(int id, String host, String nombre,
                                    String user, String pass) {
        Boolean resp = false;
        long x = database.update("CONFIGURACION",
                ConfigMOD.ValoresCONFIG(id, host, nombre, user, pass), id + "="
                        + "1", null);
        if (x == 1) {
            resp = true;
        }

        return resp;
    }


    public ConfigMOD getConfigLocal() {
        ConfigMOD _configMOD = null;
        String sql = "select * from CONFIGURACION";

        Cursor c = database.rawQuery(sql, null);

        if (c.moveToFirst()) {
            _configMOD = new ConfigMOD();
            _configMOD.setId(Integer.parseInt(c.getString(c
                    .getColumnIndex("id"))));
            _configMOD.setHost(c.getString(c.getColumnIndex("host")));
            _configMOD.setNombreBD(c.getString(c.getColumnIndex("nombreBD")));
            _configMOD.setUserBD(c.getString(c.getColumnIndex("userBD")));
            _configMOD.setPassBD(c.getString(c.getColumnIndex("passBD")));
        }

        return _configMOD;
    }

}

package MOD;

import android.content.ContentValues;

/**
 * Created by jogan1075 on 24-10-17.
 */

public class ConfigMOD {
    private int id;
    private String host;
    private String nombreBD;
    private String userBD;
    private String passBD;

    public static final String NOMBRE_TABLE_CONFIG = "CONFIGURACION";
    public static final String TABLE_CONFIG_ID = "id";
    public static final String TABLE_CONFIG_HOST = "host";
    public static final String TABLE_CONFIG_NOMBRE = "nombreBD";
    public static final String TABLE_CONFIG_USER = "userBD";
    public static final String TABLE_CONFIG_PASS = "passBD";

    public static final String SCRIPT_CREACION_TABLA_CONFIG = "CREATE TABLE "
            + NOMBRE_TABLE_CONFIG
            + "("
            + TABLE_CONFIG_ID + " INTEGER,"
            + TABLE_CONFIG_HOST + " TEXT,"
            + TABLE_CONFIG_NOMBRE + " TEXT,"
            + TABLE_CONFIG_USER + " TEXT,"
            + TABLE_CONFIG_PASS + " TEXT )";

    public static ContentValues ValoresCONFIG(int Id, String host,
                                              String nombre, String user, String pass) {
        ContentValues valores = new ContentValues();
        valores.put(TABLE_CONFIG_ID, Id);
        valores.put(TABLE_CONFIG_HOST, host);
        valores.put(TABLE_CONFIG_NOMBRE, nombre);
        valores.put(TABLE_CONFIG_USER, user);
        valores.put(TABLE_CONFIG_PASS, pass);

        return valores;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getNombreBD() {
        return nombreBD;
    }

    public void setNombreBD(String nombreBD) {
        this.nombreBD = nombreBD;
    }

    public String getUserBD() {
        return userBD;
    }

    public void setUserBD(String userBD) {
        this.userBD = userBD;
    }

    public String getPassBD() {
        return passBD;
    }

    public void setPassBD(String passBD) {
        this.passBD = passBD;
    }

}

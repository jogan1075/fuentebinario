package MOD;

import android.content.ContentValues;

/**
 * Created by jmunoz on 01-04-17.
 */

public class BoletoMOD {

    private int IdBoleto;
    private String IdUsuario;
    private String ValorTarifa;
    private String Fecha;
    private String NombreTarifa;
    private String Latitud;
    private String Longitud;
    private int Verificado;
    private int Eliminado;
    private String numFolio;


    public static final String NOMBRE_TABLE_BOLETOS = "BOLETOS";
    public static final String TABLE_BOLETOS_ID = "Id";
    public static final String TABLE_BOLETOS_IDUSUARIO = "IdUsuario";
    public static final String TABLE_BOLETOS_VALOR = "Valor";
    public static final String TABLE_BOLETOS_FECHA = "Fecha";
    public static final String TABLE_BOLETOS_NOMBRETARIFA = "IdNombreTarifa";
    public static final String TABLE_BOLETOS_LATITUD = "Latitud";
    public static final String TABLE_BOLETOS_LONGITUD = "Longitud";
    public static final String TABLE_BOLETOS_VERIFICADO = "Verificado";
    public static final String TABLE_BOLETOS_ELIMINADO = "Eliminado";
    public static final String TABLE_BOLETOS_NUMFOLIO = "NumFolio";

    public static final String SCRIPT_CREACION_TABLA_BOLETOS = "CREATE TABLE "
            + NOMBRE_TABLE_BOLETOS
            + "("
            + TABLE_BOLETOS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TABLE_BOLETOS_IDUSUARIO + " TEXT,"
            + TABLE_BOLETOS_VALOR + " TEXT,"
            + TABLE_BOLETOS_FECHA + " TEXT,"
            + TABLE_BOLETOS_NOMBRETARIFA + " TEXT,"
            + TABLE_BOLETOS_LATITUD + " TEXT,"
            + TABLE_BOLETOS_LONGITUD + " TEXT,"
            + TABLE_BOLETOS_VERIFICADO + " TEXT,"
            + TABLE_BOLETOS_ELIMINADO + " TEXT,"
            + TABLE_BOLETOS_NUMFOLIO + " TEXT )";

    public static ContentValues ValoresTarifas(String IdUsuario, String valorTarifa,
                                               String fecha, String IdnombreTarifa, String latitud,
                                               String longitud, String verificado, String eliminados,
                                               String numFolio) {
        ContentValues valores = new ContentValues();
        valores.put(TABLE_BOLETOS_IDUSUARIO,IdUsuario);
        valores.put(TABLE_BOLETOS_VALOR,valorTarifa);
        valores.put(TABLE_BOLETOS_FECHA,fecha);
        valores.put(TABLE_BOLETOS_NOMBRETARIFA,IdnombreTarifa);
        valores.put(TABLE_BOLETOS_LATITUD,latitud);
        valores.put(TABLE_BOLETOS_LONGITUD,longitud);
        valores.put(TABLE_BOLETOS_VERIFICADO,verificado);
        valores.put(TABLE_BOLETOS_ELIMINADO,eliminados);
        valores.put(TABLE_BOLETOS_NUMFOLIO,numFolio);


        return valores;
    }


    public String getNumFolio() {
        return numFolio;
    }

    public void setNumFolio(String numFolio) {
        this.numFolio = numFolio;
    }

    public int getIdBoleto() {
        return IdBoleto;
    }

    public void setIdBoleto(int idBoleto) {
        IdBoleto = idBoleto;
    }

    public String getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        IdUsuario = idUsuario;
    }


    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }


    public String getLatitud() {
        return Latitud;
    }

    public void setLatitud(String latitud) {
        Latitud = latitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLongitud(String longitud) {
        Longitud = longitud;
    }

    public int getVerificado() {
        return Verificado;
    }

    public void setVerificado(int verificado) {
        Verificado = verificado;
    }

    public int getEliminado() {
        return Eliminado;
    }

    public void setEliminado(int eliminado) {
        Eliminado = eliminado;
    }

    public String getValorTarifa() {
        return ValorTarifa;
    }

    public void setValorTarifa(String valorTarifa) {
        ValorTarifa = valorTarifa;
    }

    public String getNombreTarifa() {
        return NombreTarifa;
    }

    public void setNombreTarifa(String nombreTarifa) {
        NombreTarifa = nombreTarifa;
    }
}

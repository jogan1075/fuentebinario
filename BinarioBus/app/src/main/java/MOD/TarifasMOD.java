package MOD;

import android.content.ContentValues;

/**
 * Created by jogan1075 on 27-03-17.
 */

public class TarifasMOD {

    public  int Id;
    public  String NombreTarifa;
    public  String valor;
    public  String accion;
    public String bus;

    public static final String NOMBRE_TABLE_TARIFAS = "TARIFAS";
    public static final String TABLE_TARIFAS_ID = "Tipo_Id";
    public static final String TABLE_TARIFAS_NOMBRE = "Tipo_Nombre";
    public static final String TABLE_TARIFAS_VALOR = "Tipo_Valor";
    public static final String TABLE_TARIFAS_ACCION = "Tipo_Accion";
    public static final String TABLE_TARIFAS_BUS = "Tipo_Bus";

    public static final String SCRIPT_CREACION_TABLA_TARIFAS = "CREATE TABLE "
            + NOMBRE_TABLE_TARIFAS
            + "("
            + TABLE_TARIFAS_ID + " INTEGER PRIMARY KEY,"
            + TABLE_TARIFAS_NOMBRE + " TEXT,"
            + TABLE_TARIFAS_VALOR + " TEXT,"
            + TABLE_TARIFAS_ACCION + " TEXT,"
            + TABLE_TARIFAS_BUS + " TEXT )";

    public static ContentValues ValoresTarifas(int Id, String NombreTarifa,String valor, String accion, String bus) {
        ContentValues valores = new ContentValues();
        valores.put(TABLE_TARIFAS_ID, Id);
        valores.put(TABLE_TARIFAS_NOMBRE, NombreTarifa);
        valores.put(TABLE_TARIFAS_VALOR, valor);
        valores.put(TABLE_TARIFAS_ACCION, accion);
        valores.put(TABLE_TARIFAS_BUS, bus);

        return valores;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNombreTarifa() {
        return NombreTarifa;
    }

    public void setNombreTarifa(String nombreTarifa) {
        NombreTarifa = nombreTarifa;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }
}

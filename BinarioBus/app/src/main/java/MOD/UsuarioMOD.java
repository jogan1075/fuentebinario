package MOD;


public class UsuarioMOD
{
	private int Id;
	private String Nombre;
	private String Email;
	private String Password;
	private int IdEmpresaAsignada;
	private String Logo;
	private String NroBus;
	private String ultimoBoletoVendido;
	private String politicaSeguridad;

	public String getPoliticaSeguridad() {
		return politicaSeguridad;
	}

	public void setPoliticaSeguridad(String politicaSeguridad) {
		this.politicaSeguridad = politicaSeguridad;
	}

	public String getUltimoBoletoVendido() {
		return ultimoBoletoVendido;
	}

	public void setUltimoBoletoVendido(String ultimoBoletoVendido) {
		this.ultimoBoletoVendido = ultimoBoletoVendido;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public int getIdEmpresaAsignada() {
		return IdEmpresaAsignada;
	}

	public void setIdEmpresaAsignada(int idEmpresaAsignada) {
		IdEmpresaAsignada = idEmpresaAsignada;
	}

	public String getLogo() {
		return Logo;
	}

	public void setLogo(String logo) {
		Logo = logo;
	}

	public String getNroBus() {
		return NroBus;
	}

	public void setNroBus(String nroBus) {
		NroBus = nroBus;
	}
}
